/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 16:41:09 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/13 16:53:52 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				print_arg_error(void)
{
	ft_putendl_fd("ft_ls: fts_open: No such file or directory", 2);
	return (0);
}

int				print_flag_error(char flag)
{
	const char	*s[] = {
		"ft_ls: illegal option -- ", "usage: ft_ls [-", "] [file ...]"
	};
	char		flag_str[2];
	char		t1[ft_strlen(s[0]) + 2];
	char		t[ft_strlen(s[1]) + ft_strlen(g_flags) + ft_strlen(s[2]) + 1];

	ft_putendl_fd(ft_strcat(ft_strcpy(t1, s[0]), ft_ctoa(flag_str, flag)), 2);
	ft_putendl_fd(ft_strcat(ft_strcat(ft_strcpy(t, s[1]), g_flags),
	(char *)s[2]), 2);
	return (0);
}

int				print_file_error(char *file)
{
	const char	*s[] = { "ft_ls: ", ": No such file or directory" };
	char		t[ft_strlen(s[0]) + ft_strlen(file) + ft_strlen(s[1]) + 1];

	ft_putendl_fd(ft_strcat(ft_strcat(ft_strcpy(t, s[0]), file), (char *)s[1]),
	2);
	return (1);
}

int				print_permission_error(char *file)
{
	const char	*s[] = { "ft_ls: ", ": Permission denied" };
	char		t[ft_strlen(s[0]) + ft_strlen(file) + ft_strlen(s[1]) + 1];
	char		**splitted;
	int			i;

	i = 0;
	splitted = ft_strsplit(file, '/');
	while (splitted[i + 1] != NULL)
		free(splitted[i++]);
	ft_putendl_fd(ft_strcat(ft_strcat(ft_strcpy(t, s[0]), splitted[i]),
	(char *)s[1]), 2);
	free(splitted[i]);
	free(splitted);
	return (1);
}
