/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elem_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 11:26:55 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/16 11:27:20 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem				*new_elem(void)
{
	t_elem			*e;

	e = (t_elem *)malloc(sizeof(t_elem));
	e->name = "";
	e->type = 0;
	e->last = 0;
	e->max = 0;
	e->next = NULL;
	return (e);
}

void				fill_elem(struct dirent *d, t_elem *f, t_elem *e, char *n)
{
	struct stat		buffer;

	lstat(n, &buffer);
	e->name = ft_strdup(d->d_name);
	e->type = d->d_type;
	e->last = buffer.st_mtime;
	f->max = ft_strlen(d->d_name) > f->max ? ft_strlen(d->d_name) : f->max;
}

void				free_elems(t_elem *list)
{
	t_elem			*tmp;

	while (list != NULL)
	{
		tmp = list;
		list = list->next;
		if (ft_strcmp(tmp->name, "") != 0)
			free(tmp->name);
		free(tmp);
	}
}
