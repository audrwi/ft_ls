/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 14:36:18 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/16 11:28:51 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <sys/types.h>
# include <sys/stat.h>
# include <dirent.h>
# include <time.h>
# include "libft/libft.h"

typedef struct		s_infos
{
	char			**from[3];
	int				rec;
	char			flags[4];
}					t_infos;

typedef struct		s_elem
{
	char			*name;
	int				type;
	time_t			last;
	size_t			max;
	struct s_elem	*next;
}					t_elem;

t_elem				*new_elem(void);
void				fill_elem(struct dirent *d, t_elem *f, t_elem *e, char *n);
void				free_elems(t_elem *list);
char				*get_entire_name(char *folder, char *file);
int					is_before(char *flags, char *current, char *before);
char				**sort_by_ascii(char **array, int j, char *flags);
void				update_return(int value);
int					print_arg_error(void);
int					print_flag_error(char flag);
int					print_file_error(char *file);
int					print_permission_error(char *file);
t_elem				*rec_list(char *file, char *flags, int space, t_elem *e);
t_elem				*list(char *file, char *flags, int space, t_elem *e);

static int			g_return = 0;
static char			g_flags[] = "1Ralrt";
static t_elem		*(*g_list[2])(char *n, char *f, int s, t_elem *e) = {
	list, rec_list
};
static t_infos		g_infos = {
	.from = { NULL, NULL, NULL }, .rec = 0, .flags = ""
};

#endif
