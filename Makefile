# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/06 14:32:18 by aboucher          #+#    #+#              #
#    Updated: 2017/05/16 11:47:24 by aboucher         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

SRC = main.c		\
	  lists.c		\
	  elems.c		\
	  functions.c	\
	  errors.c

OBJ = $(SRC:.c=.o)

HEAD = ft_ls.h

FLAGS = -Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ) makelibft
	@clang $(OBJ) libft/libft.a $(MLX) -o $(NAME)
	@echo "\rCompiling project...	\033[0;32m[OK]\033[0m"
	@echo "\033[0;36m"
	@echo "███████╗████████╗     ██╗     ███████╗"
	@echo "██╔════╝╚══██╔══╝     ██║     ██╔════╝"
	@echo "█████╗     ██║        ██║     ███████╗"
	@echo "██╔══╝     ██║        ██║     ╚════██║"
	@echo "██║        ██║███████╗███████╗███████║"
	@echo "╚═╝        ╚═╝╚══════╝╚══════╝╚══════╝"
	@echo "\033[0m"

%.o: %.c $(HEAD)
	@clang $(FLAGS) -o $@ -c $< -I$(HEAD)

makelibft:
	@make -j -C libft/

clean:
	@echo "Cleaning objects...	\c"
	@rm -f $(OBJ)
	@make -C libft/ clean
	@echo "\rCleaning objects...	\033[0;32m[OK]\033[0m"

fclean: clean
	@echo "Cleaning target...	\c"
	@rm -f $(NAME)
	@make -C libft/ fclean
	@echo "\rCleaning target...	\033[0;32m[OK]\033[0m"

re: fclean all

.PHONY: all makelibft clean fclean re
