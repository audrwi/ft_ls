/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:59:42 by aboucher          #+#    #+#             */
/*   Updated: 2015/11/29 17:27:42 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		i;
	int		j;
	int		dst;

	i = 0;
	dst = 0;
	while (str[i] >= 9 && str[i] <= 32)
		i++;
	j = i;
	while (str[j])
	{
		if (j == i && (str[j] == 43 || str[j] == 45))
			dst = 0;
		else if ((str[j] < 48 || str[j] > 57) && str[i] == 45)
			return (-dst);
		else if (str[j] < 48 || str[j] > 57)
			return (dst);
		else
			dst = dst * 10 + (str[j] - 48);
		j++;
	}
	if (str[i] == 45)
		return (-dst);
	return (dst);
}
