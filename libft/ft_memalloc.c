/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 23:32:21 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/06 02:14:58 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*zone;

	zone = (char *)malloc(sizeof(char) * size);
	if (zone == NULL)
		return (NULL);
	zone = ft_memset(zone, '\0', size);
	return (zone);
}
