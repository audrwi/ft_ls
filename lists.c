/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/01 18:35:19 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/16 11:51:45 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			print_element(char *name, char *flags)
{
	if (name[0] != '.' || (name[0] == '.' && name[1] != '/' &&
	ft_strnchr(flags, 'a')))
		ft_putendl(name);
	return (1);
}

static t_elem		*get_dir_content(DIR *dir, char *flags, t_elem *r, char *f)
{
	struct dirent	*d;
	t_elem			*tmp;
	t_elem			*tmp2;
	char			*s[2];

	while ((d = readdir(dir)) != NULL)
	{
		tmp = r;
		s[0] = get_entire_name(f, d->d_name);
		while (tmp->next && (s[1] = get_entire_name(f, tmp->next->name)))
			if (is_before(flags, s[1], s[0]) && (tmp = tmp->next))
				free(s[1]);
			else
				break ;
		if (tmp->next)
			free(s[1]);
		tmp2 = tmp->next ? tmp->next : NULL;
		tmp->next = new_elem();
		tmp = tmp->next;
		fill_elem(d, r, tmp, s[0]);
		free(s[0]);
		tmp->next = tmp2;
	}
	closedir(dir);
	return (r);
}

t_elem				*rec_list(char *file, char *flags, int space, t_elem *e)
{
	t_elem			*tmp;
	char			*s;

	e = list(file, flags, space, NULL);
	tmp = e;
	while (e && (e = e->next))
		if (e->type == 4 && ft_strcmp(e->name, ".") != 0 && ft_strcmp(e->name,
			"..") != 0 && (e->name[0] != '.' || ft_strnchr(flags, 'a')))
		{
			s = get_entire_name(file, e->name);
			rec_list(s, flags, 2, NULL);
			free(s);
		}
	free_elems(tmp);
	return (NULL);
}

t_elem				*list(char *file, char *f, int space, t_elem *e)
{
	DIR				*d;
	char			ret[ft_strlen(file) + 2];
	char			*cpy;
	t_elem			*tmp;
	struct stat		buffer;

	tmp = NULL;
	if (stat(file, &buffer) == 0 && S_ISDIR(buffer.st_mode))
	{
		if ((cpy = ft_strdup(file)) && space >= 0 && ft_strcpy(ret, "\n"))
			ft_putendl(ft_strcat((space ? ft_strcat(ret, file) : file), ":"));
		if ((d = opendir(cpy)))
		{
			e = get_dir_content(d, f, new_elem(), cpy);
			tmp = e;
			while ((e = e->next))
				print_element(e->name, f);
		}
		else
			update_return(print_permission_error((char *)cpy));
		free(cpy);
	}
	else
		print_element(file, f);
	return (tmp);
}
