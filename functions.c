/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/12 20:01:22 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/16 11:29:49 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char				*get_entire_name(char *folder, char *file)
{
	char			*str;

	str = ft_memalloc(ft_strlen(folder) + ft_strlen(file) + 2);
	ft_strcat(ft_strcpy(ft_strcpy(str, folder),ft_strcat(str, "/")), file);
	return (str);
}

int					is_before(char *flags, char *current, char *before)
{
	const int		t = ft_strnchr(flags, 't');
	const int		r = ft_strnchr(flags, 'r');
	struct stat		buffer;
	struct stat		buffer2;

	lstat(current, &buffer);
	lstat(before, &buffer2);
	if ((t && ((!r && buffer.st_mtime > buffer2.st_mtime) || (r &&
	buffer.st_mtime < buffer2.st_mtime) || (buffer.st_mtime ==
	buffer2.st_mtime && ft_strcmp(before, current) > 0))) || (!t && ((!r &&
	ft_strcmp(before, current) > 0) || (r && ft_strcmp(before, current) < 0))))
		return (1);
	return (0);
}

char				**sort_by_ascii(char **array, int j, char *flags)
{
	int				i;
	char			*tmp;

	i = 0;
	while (j > 0 && array && array[i] != NULL)
	{
		if (i > 0 && is_before(flags, array[i], array[i - 1]))
		{
			tmp = array[i];
			array[i] = array[i - 1];
			array[i - 1] = tmp;
			i--;
		}
		else
			i++;
	}
	return (array);
}
