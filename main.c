/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 14:32:36 by aboucher          #+#    #+#             */
/*   Updated: 2017/05/16 11:04:31 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		add_flag(char c)
{
	int			i;

	i = 0;
	if (c == 'R')
		g_infos.rec = 1;
	else
	{
		while (g_infos.flags[i])
			i++;
		g_infos.flags[i] = c;
	}
}

static int		is_valid_argument(char *arg)
{
	int			i;

	i = 0;
	if (ft_strcmp(arg, "") == 0)
		return (print_arg_error());
	else if (arg[0] != '-')
		return (0);
	while (arg[++i])
		if (!ft_strnchr(g_flags, arg[i]))
			return (print_flag_error(arg[i]));
		else if (arg[i] != '1' && !ft_strnchr(g_infos.flags, arg[i]))
			add_flag(arg[i]);
	return (1);
}

static int		check_file_validity(char *file, int max, int n)
{
	int			i;
	struct stat	buffer;
	const int	valid = (stat(file, &buffer) == 0);
	const int	type = (valid ? S_ISDIR(buffer.st_mode) + 1 : 0);

	i = 0;
	if (g_infos.from[type] != NULL)
		while (g_infos.from[type][i] != NULL)
			i++;
	else
		g_infos.from[type] = (char **)malloc(sizeof(char *) * (max + 1));
	g_infos.from[type][i] = ft_strdup(file);
	g_infos.from[type][i + 1] = NULL;
	if ((g_infos.from[1] != NULL && g_infos.from[2] != NULL &&
		g_infos.from[1][0] && g_infos.from[2][0]) || (g_infos.from[2] != NULL &&
		g_infos.from[2][0] && g_infos.from[2][1]) || (g_infos.from[0] != NULL &&
		g_infos.from[0][0]))
		n = 0;
	return (n);
}

void			update_return(int value)
{
	g_return = value;
}

int				main(int ac, char **av)
{
	static int	i = -1;
	static int	j = -1;

	while (--ac && ((*++av)[0] == '-' || ft_strcmp(*av, "") == 0))
		if (ft_strcmp(*av, "--") == 0 && *av++)
			break ;
		else if (!is_valid_argument(*av))
			return (1);
	if (!(ac && *av))
		free_elems(g_list[g_infos.rec](".", g_infos.flags, i, NULL));
	while (ac && *av)
		i = check_file_validity(*av++, ac--, i);
	while (++j <= 2 && ((g_infos.from[j] = sort_by_ascii(g_infos.from[j], j,
	g_infos.flags)) || !g_infos.from[j]))
		while (g_infos.from[j] != NULL && *g_infos.from[j] != NULL)
		{
			if (j == 0)
				g_return = print_file_error(*g_infos.from[j]);
			else
				free_elems(g_list[g_infos.rec](*g_infos.from[j], g_infos.flags,
				i++, NULL));
			free(*g_infos.from[j]++);
		}
	return (g_return);
}
